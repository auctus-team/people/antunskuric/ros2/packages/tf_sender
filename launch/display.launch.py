from launch import LaunchDescription
from launch_ros.actions import Node
import os
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():

    config_rviz = os.path.join(
        get_package_share_directory('tf_sender'),
        'launch',
        'rviz.rviz'
        )

    return LaunchDescription([
        Node(
            package='tf_sender',
            namespace='',
            executable='tf_sender_node',
            name='tf_sender_node'
        ),
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', config_rviz]
        )
    ])