import rclpy
import rclpy.node


from tf2_ros import TransformBroadcaster
import geometry_msgs.msg


# Node instance that is shared to all the function
# ideal this node code would be a pyhton class 
# then the node variable would become self 
node = None

def send_tf_msg(br, p,q,my_frame,world_frame):
    global node
    # create tf message
    t = geometry_msgs.msg.TransformStamped()

    # for timestamp you need the node instance
    t.header.stamp = node.get_clock().now().to_msg()
    t.header.frame_id = world_frame
    t.child_frame_id = my_frame
    t.transform.translation.x = p[0]
    t.transform.translation.y = p[1]
    t.transform.translation.z = p[2]
    t.transform.rotation.x = q[0]
    t.transform.rotation.y = q[1]
    t.transform.rotation.z = q[2]
    t.transform.rotation.w = q[3]
    # send the message
    br.sendTransform(t)

def main():
    global node
    print('Hi from tf_sinder_node.')

    rclpy.init()
    node = rclpy.create_node('tf_sender_node')

    # instatiate the tf sender
    # for TransformBroadcaster you need the node instance
    broadcaster = TransformBroadcaster(node)

    pos = [0.0, 0.1, 0.5]
    quat = [0.0, 0.0, 0.0, 1.0]
    while True:
        send_tf_msg(broadcaster, pos,  quat, 'my_tf', "world")
               


if __name__ == '__main__':
    main()
